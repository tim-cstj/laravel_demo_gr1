var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
 mix.browserSync({
         online: false,
         proxy : 'localhost:8000'
   });

   var bowerPath='../bower_components/';

  // Sass
  var options = {
      includePaths: [
          'resources/assets/bower_components/foundation/scss',
          'resources/assets/bower_components/motion-ui/src'
      ]
  };

  mix.sass('app.scss', null, options);

  // Javascript
  var jQuery = bowerPath+'jquery/dist/jquery.js';
  var foundationJsFolder = bowerPath+'foundation/js/foundation/';

  mix.scripts([
     jQuery,
     foundationJsFolder + 'foundation.js',
     foundationJsFolder + 'foundation.topbar.js',
     foundationJsFolder + 'foundation.alert.js',
     foundationJsFolder + 'foundation.reveal.js',
     foundationJsFolder + 'foundation.tooltip.js',
     foundationJsFolder + 'foundation.accordion.js',
     'start_foundation.js'
  ])
  .scripts([
    bowerPath+'modernizr/modernizr.js'
  ],'public/js/modernizr.js');

  mix.version(['css/app.css','js/all.js']);
});
