<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserTableSeeder::class);


        factory('App\User',25)->create();

        $user=new User(['name'=>'Luc Vadnais',
        'email'=>'lvadnais@cstj.qc.ca',
        'password'=>Hash::make('1234567')
      ]);

      $user->save();
    }
}
