<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function show(){
      $users=User::all();
    return view('welcome',['users'=>$users]);

    }

    public function view(User $user){
      return view('user',['user'=>$user]);
    }
}
