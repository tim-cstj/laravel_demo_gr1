# Production en ligne
## Fichiers de démarrage à partir de Laravel 5.2

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Documentation officielle
Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Démarrage de projet
### Au début du projet:
* Configurer votre accès git grâce à config.bat
* Cloner le repositoire
* Installer les dépendances composer (composer install)
* Installer les dépendances nodeJs (npm install)
* Installer les dépendances bower (bower install)
* Créer une bd sous phpMyAdmin
* Créer un fichier de configuration .env à la racine et configurer. (debug, timezone, locale, bd ...). **Veuillez conserver une copie du fichier .env puisque celui-ci n'est jamais ajouté au git.**
* Générer une clé d'encodage unique !!une fois seulement!! (php artisan key:generate) **(ne plus refaire par la suite)**

### À chaque début de cours:
* Configurer votre accès git grâce à config.bat
* Créer la bd et exécuter la migration
* Si le projet est déjà installé sur votre ordinateur:
  * Faire un pull depuis Bitbucket
* Si le projet n'est pas installé sur votre ordinateur:
  * Cloner le projet à nouveau et installer les dépendances (ne pas installer de nouvelle clé d'encodage)